export const postsReducer = (state = null, action) => {
    switch (action.type) {
        case "SET":
            return action.posts;
        case "ADD":
            return [...state, action.post];
        case "DELETE":
            return state.filter(el => el._id !== action.id);
        case "EDIT":
            return state.map(post =>
                post.id === action.post.id ? state[post] = action.post : state
            );
        default:
            return state;

    }
};