import { createStore, combineReducers } from "redux";
import initialData from './initialData';
import { postsReducer } from './reducers';

const reducers = combineReducers({
    posts: postsReducer
})

export default createStore(
    (state, action) => reducers(state, action),
    initialData
);