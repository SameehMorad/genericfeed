export const setPosts = posts => ({
    type: "SET",
    posts
});
export const addPost = post => ({
    type: "ADD",
    post
});

export const deletePost = id => ({
    type: "DELETE",
    id
});

export const editPost = post => ({
    type: "EDIT",
    post
});