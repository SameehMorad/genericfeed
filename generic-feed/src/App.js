import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Navbar from "./components/navbar.component";
import PostsList from "./components/posts-list.component";
import EditPost from "./components/edit-post.component";
import CreatePost from "./components/create-post.component";
import Grid from '@material-ui/core/Grid';


function App() {
  return (
    <Router>
      <Navbar />
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
      >
        <br />
        <Route path="/" exact component={PostsList} />
        <Route path="/edit/:id" exact component={EditPost} />
        <Route path="/create" exact component={CreatePost} />
      </Grid>
    </Router>
  );
}

export default App;
