import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CreatePost from "../components/create-post.component";
import Post from "../components/post.component";
import socketIOClient from 'socket.io-client';
import { useSelector, useDispatch } from "react-redux";
import { setPosts, deletePost, addPost } from "../store/actions";


const PostsList = () => {
    const [posts, setPostss] = useState([]);
    const postsRedux = useSelector(state => state.posts);
    const dispatch = useDispatch();

    useEffect(() => {
        axios.get('http://localhost:5000/posts')
            .then(res => {
                dispatch(setPosts(res.data))
            }).catch(error => {
                console.log(error);
            });

        const socket = socketIOClient('http://localhost:8000');
        socket.on("newPost", post => {
            dispatch(addPost(post));
        });

        socket.on("postDeleted", id => {
            console.log(id);
            //   setPosts(posts.filter(el => el._id !== id))
            dispatch(deletePost(id))
        });

    }, []);

    function deletePostList(id) {
        axios.delete('http://localhost:5000/posts/' + id)
            .then(res => console.log(res));
    }

    function postsList() {
        postsRedux.reverse();
        return postsRedux.map(currPost => {
            return <Post post={currPost} deletePost={deletePostList} key={currPost._id} />
        })
    }

    function handleScroll(e) {
        console.log("here")
        const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
        if (bottom) { console.log("yessssssssssssssssssssssss") }

    }

    return (
        <div>
            <div>
                <CreatePost />
            </div>
            <div>
                {postsRedux.length}
            </div>
            <div className="posts" onScroll={handleScroll}>
                {postsList()}
            </div>
        </div>
    );
}

export default PostsList;