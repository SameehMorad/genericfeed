import React, { useState } from 'react';
import axios from 'axios';
import PostAddIcon from '@material-ui/icons/PostAdd';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import { makeStyles } from '@material-ui/core/styles';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import { Card } from '@material-ui/core';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import ChipInput from 'material-ui-chip-input'

const useStyles = makeStyles((theme) => ({
    card: {
        width: 600
    },
    root: {
        width: 600,
        padding: 10,
        marginTop: 15

    },
    paper: {
        maxWidth: 600,
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        height: 90
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    upload: {
        display: 'none',
    },
    tag: {
        width: 500,
        margin: 5,
    }
}));

const CreatePost = () => {
    const [content, setContent] = useState('');
    const [file, setFile] = useState(null);
    const [display, setDisplay] = useState(null);
    const [tags, setTags] = useState([]);

    const classes = useStyles();

    function onChangeContent(e) {
        setContent(e.target.value)
    }

    function onSubmit(e) {
        e.preventDefault();


        const post = {
            content,
            date: new Date(),
            tags
        }

        const json = JSON.stringify(post);
        const blob = new Blob([json], {
            type: 'application/json'
        });
        const data = new FormData();
        data.append("post", blob);
        data.append("file", file);

        // refactor later
        data.append("tags", tags);

        axios.post('http://localhost:5000/posts/add', data)
            .then(res => console.log(res));

    }

    function handleFileChange(e) {
        setFile(e.target.files[0]);
        setDisplay(URL.createObjectURL(e.target.files[0]))
    }


    function checkFileType() {
        return file?.name.split(".")[1] === "mp4"
    }

    function handleDelete(i) {
        setTags(tags.filter((tag, index) => index !== i));
    }

    function handleAddition(tag) {
        setTags([...tags, tag]);
    }


    return (
        <Card variant="outlined" className={classes.root}>
            <CardMedia >
                <video
                    poster={display}
                    controls={checkFileType()}
                    autoPlay
                    height="600"
                    width="600"
                >

                    <source src={display} type="video/mp4" />
                </video>
            </CardMedia>
            <CardContent>

                <Paper component="form" onSubmit={onSubmit} className={classes.paper}>
                    <InputBase
                        className={classes.input}
                        placeholder="New Post!"
                        type="text"
                        required
                        value={content}
                        onChange={onChangeContent}
                    >

                    </InputBase>

                    <IconButton type="submit">
                        <PostAddIcon />
                    </IconButton>

                    <input accept="image/*" className={classes.upload} id="icon-button-file" type="file" onChange={handleFileChange} required />
                    <label htmlFor="icon-button-file">
                        <IconButton aria-label="upload picture" component="span">
                            <AttachFileIcon />
                        </IconButton>
                    </label>

                </Paper>

                <ChipInput
                    className={classes.tag}
                    value={tags}
                    onAdd={(chip) => handleAddition(chip)}
                    onDelete={(chip, index) => handleDelete(index)}
                    placeholder="Tags"
                />

            </CardContent>
        </Card>
    );
}

export default CreatePost;