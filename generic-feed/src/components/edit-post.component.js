import React, { useState, useEffect } from 'react';
import axios from 'axios';
import DoneIcon from '@material-ui/icons/Done';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
}));


const EditPost = props => {
    const [content, setContent] = useState('');
    const classes = useStyles();

    useEffect(() => {
        axios.get('http://localhost:5000/posts/' + props.match.params.id)
            .then(res => {
                setContent(res.data.content);
            }).catch(err => {
                console.log(err);
            });

    }, []);

    function onChangeContent(e) {
        setContent(e.target.value)
    }

    function onSubmit(e) {
        e.preventDefault();

        const post = {
            content,
            date: new Date()
        }

        console.log(post);

        axios.post('http://localhost:5000/posts/update/' + props.match.params.id, post)
            .then(res => console.log(res));

        window.location = "/";

    }


    return (
        <Paper component="form" onSubmit={onSubmit} className={classes.root}>
            <InputBase
                className={classes.input}
                placeholder="New Post!"
                type="text"
                required
                value={content}
                onChange={onChangeContent}
            />

            <IconButton type="submit">
                <DoneIcon />
            </IconButton>
        </Paper>

    );


}

export default EditPost;
