import React from 'react';
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { Card } from '@material-ui/core';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import EditIcon from '@material-ui/icons/Edit';
import { TimeAgo } from '@n1ru4l/react-time-ago';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles((theme) => ({
    root: {
        width: 600,
        padding: 10,
        marginTop: 15
    },
    chip: {
        margin: theme.spacing(0.5),
    },

}));

const Post = props => {
    const classes = useStyles();

    function checkFileType() {
        return props.post.mediaUrl.split(".")[1] === "mp4"
    }

    function handleChipClick(data) {

    }

    return (
        <Card className={classes.root} variant="outlined">
            <CardHeader
                avatar={
                    <Avatar aria-label="recipe">
                        S
                    </Avatar>
                }

                title="username"
                subheader={
                    <TimeAgo
                        date={new Date(props.post.date)}
                        render={({ error, value }) => <span>{value}</span>}
                    />}
            />
            <CardMedia>
                <video
                    poster={'http://localhost:5000/posts/media/' + props.post.mediaUrl}
                    controls={checkFileType()}
                    height="600"
                    width="600" >

                    <source src={'http://localhost:5000/posts/media/' + props.post.mediaUrl} type="video/mp4" />
                </video>
            </CardMedia>
            <CardContent>

                <Typography variant="body2" color="textSecondary" component="p">
                    {props.post.content}
                </Typography>

                <div>
                    {props.post.tags.split(',').map((data) => {
                        return (
                            <Chip label={data} className={classes.chip} onClick={handleChipClick} />
                        );
                    })}
                </div>
            </CardContent>

            <CardActions>
                <IconButton edge="start">
                    <DeleteIcon href='#' onClick={() => { props.deletePost(props.post._id) }} />
                </IconButton>

                <Link to={"/edit/" + props.post._id}>
                    <IconButton edge="start">
                        <EditIcon />
                    </IconButton>
                </Link>
            </CardActions>
        </Card >
    )
}

export default Post;