const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);


require('dotenv').config();


const port = /*proccess.env.PORT ||*/ 5000;
const socketPort = 8000;


app.use(cors());
app.use(express.json());
app.use(fileUpload());

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }
);

const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connection established successfully");
});

const postsRouter = require('./routes/posts')(io);
const usersRouter = require('./routes/users');

app.use('/posts', postsRouter);
app.use('/users', usersRouter);

io.listen(socketPort);
console.log('listening on port ', socketPort);
app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});