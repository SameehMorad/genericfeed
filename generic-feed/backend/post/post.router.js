const PostController = require('./post.controller');
const router = require('express').Router();

router.get('/', PostController.getAllPosts);
router.post('/', PostController.addPost);
router.delete('/:id', PostController.deletePost);
router.post('/:id', PostController.updatePost);
router.get('/media/:mediaUrl', PostController.getMedia);

module.exports = router;