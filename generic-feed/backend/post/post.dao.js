const Post = require('./post.model');

async function getAllPosts() {
    //to sort by date
    return await Post.find({});
}

async function addPost(post) {
    return post.save();
}

async function deletePost(id) {
    return await Post.findByIdAndDelete(id)
}

async function updatePost(post) {
    // findandmodify or find and update
    return await Post.findByIdAndUpdate(post.id, post)
}

module.exports = {
    getAllPosts,
    addPost,
    deletePost,
    updatePost
};