const PostDao = require('./post.dao');
const Post = require('./post.model');

async function getAllPosts(req, res) {
    try {
        const posts = await PostDao.getAllPosts();
        res.json(posts);
    }
    catch (err) {
        res.status(400).json('Error: ' + err);
    }
};

async function addPost(req, res) {

    try {
        const content = JSON.parse(req.files.post.data.toString()).content;
        const date = JSON.parse(req.files.post.data.toString()).date;
        const tags = req.body.tags;

        // refactor later
        const file = req.files.file;
        const dir = __dirname;


        file.mv(`${dir}/uploads/${file.name}`);

        const mediaUrl = file.name;

        const newPost = new Post({ content, date, mediaUrl, tags });
        await PostDao.addPost(newPost);
    }
    catch (err) {
        res.status(400).json('Error: ' + err);
    }
}

async function deletePost(req, res) {
    try {
        await PostDao.deletePost(req.params.id);
    }
    catch (err) {
        res.status(400).json('Error: ' + err);
    }
}

async function updatePost(req, res) {
    try {
        await PostDao.updatePost(req.body.post);
    }
    catch (err) {
        res.status(400).json('Error: ' + err);
    }
}


async function getMedia(req, res) {
    res.sendFile(`${__dirname}/uploads/${req.params.mediaUrl}`);
}


module.exports = {
    getAllPosts,
    addPost,
    deletePost,
    updatePost,
    getMedia
};