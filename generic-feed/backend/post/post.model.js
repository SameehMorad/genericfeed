const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const postSchema = new Schema({
    content: { type: String, required: true, trim: true },
    date: { type: Date, required: true },
    mediaUrl: { type: String },
    tags: { type: String }
});

const Post = mongoose.model('Post', postSchema);

module.exports = Post