const router = require('express').Router();
let Post = require('../models/post.model');


module.exports = (io) => {

    router.route('/').get((req, res) => {
        Post.find()
            .then(posts => res.json(posts))
            .catch(err => res.status(400).json('Error: ' + err));
    });

    router.route('/add').post((req, res) => {
        const content = JSON.parse(req.files.post.data.toString()).content;
        const date = JSON.parse(req.files.post.data.toString()).date;
        const tags = req.body.tags;

        // refactor later
        const file = req.files.file;
        const dir = __dirname;

        file.mv(`${dir}/uploads/${file.name}`, err => {
            if (err) {
                console.error(err);
                return res.status(400).json('Error: ' + err);
            }
        });

        const mediaUrl = file.name;

        const newPost = new Post({ content, date, mediaUrl, tags });

        newPost.save()
            .then(() => {
                // res.json('Post added!')
                // socket response

                console.log("hiii")

                io.emit('newPost', newPost);
                console.log("byeee")

            })
            .catch(err => {
                console.log(err);
                res.status(400).json('Error: ' + err)

            });

    });

    router.route('/:id').get((req, res) => {
        Post.findById(req.params.id)
            .then(post => res.json(post))
            .catch(err => res.status(400).json('Error: ' + err));
    });

    router.route('/media/:mediaUrl').get((req, res) => {
        res.sendFile(`${__dirname}/uploads/${req.params.mediaUrl}`);
    });

    router.route('/:id').delete((req, res) => {
        Post.findByIdAndDelete(req.params.id)
            .then(() => {
                io.emit('postDeleted', req.params.id);
            })
            .catch(err => res.status(400).json('Error: ' + err));
    });

    router.route('/update/:id').post((req, res) => {
        Post.findById(req.params.id)
            .then(post => {
                post.content = req.body.content;
                post.date = Date.parse(req.body.date);

                post.save()
                    .then(() => {

                        io.emit('postEditted')
                        // res.json('Post updated!')

                    })
                    .catch(err => res.status(400).json('Error: ' + err));
            })
            .catch(err => res.status(400).json('Error: ' + err));
    });
    return router;
}